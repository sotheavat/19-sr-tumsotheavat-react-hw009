import './App.css';
import NavMenu from "./component/NavMenu";
import {BrowserRouter as Router,Route,Switch} from "react-router-dom";
import Home from "./page/Home";
import Welcome from "./page/Welcome";
import Video from "./page/VIdeo";
import Account from "./page/Account";
import Auth from "./page/Auth";
import {useRef, useState} from "react";
import DisplayCardById from "./component/DisplayCardById";

function App() {
    const data = useRef([{
        name:"Lamborghini",
        color:"Red",
        description:"Automobili Lamborghini S.p.A. is an Italian brand and manufacturer of luxury sports cars and SUVs based in Sant'Agata Bolognese. The company is owned by the Volkswagen Group through its subsidiary Audi.",
        image:"https://i.pinimg.com/originals/6d/b7/ae/6db7ae4c1905457bdaa134f73fb510c2.png"
    },{
        name:"BMW",
        color:"Blue",
        description:"Bayerische Motoren Werke AG, commonly referred to as BMW, is a German multinational corporation which produces luxury vehicles and motorcycles. The company was founded in 1916 as a manufacturer of aircraft engines, which it produced from 1917 until 1918 and again from 1933 to 1945.",
        image:"https://imgd.aeplcdn.com/0x0/n/cw/ec/41406/bmw-8-series-right-front-three-quarter8.jpeg"
    },{
        name:"Bugatti",
        color:"Black",
        description:"Automobiles Ettore Bugatti was a French car manufacturer of high-performance automobiles, founded in 1909 in the then-German city of Molsheim, Alsace by the Italian-born industrial designer Ettore Bugatti. The cars were known for their design beauty and for their many race victories",
        image:"https://purepng.com/public/uploads/large/purepng.com-black-bugatti-veyron-grand-sport-vitesse-carcarvehicletransportbugatti-961524667121s2ooa.png"
    },{
        name:"Ferrari",
        color:"Yello",
        description:"Ferrari S.p.A. is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 out of the Alfa Romeo race division as Auto Avio Costruzioni, the company built its first car in 1940, and produced its first Ferrari-badged car in 1947",
        image:"https://i.pinimg.com/originals/9d/c4/5b/9dc45b3db2b335c118e54a873756fb1b.png"
    }]);
    const [isLoggedIn,setIsLoggedIn] = useState(false);
  return (
    <div className="defaultH1">
        <Router>
            <NavMenu/>
            <Switch>
                <Route exact path="/">
                    <Home data={data.current}/>
                </Route>
                <Route exact path="/auth" render={()=><Auth login={{
                    setIsLoggedIn: setIsLoggedIn,
                    isLoggedIn: isLoggedIn
                }}/>}/>
                <Route exact path="/DisplayCardById/:id" >
                    <DisplayCardById data={data.current}/>
                </Route>
                <Route path="/video" component={Video}/>
                <Route path="/account" component={Account}/>
                <Route exact path="/welcome" render={() => isLoggedIn ?
                    (<Welcome setIsLoggedIn={setIsLoggedIn}/>)
                    :
                    (<Auth login={{
                        setIsLoggedIn: setIsLoggedIn,
                        isLoggedIn: isLoggedIn
                    }}/>)
                }/>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
